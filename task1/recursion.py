from math import factorial


def combinator(n: int, k: int) -> int:
    """Функция нахождения числа сочетаний
    n-элементного множества по k элементов"""

    return int(factorial(n)/(factorial(k) * factorial(n - k)))


def recursion(n: int) -> int:
    """Функция рекурсивного нахождения количества
    неправильных шеренг в зависимости от n"""

    global x_list
    k_list = [i for i in range(n + 1)]

    result = 0
    for k in k_list:
        try:
            result += 1 / 4 * combinator(n, k) * x_list[k] * x_list[n - k]
        except IndexError:
            x_list.append(recursion((n - k) - 1))
            result += 1 / 4 * combinator(n, k) * x_list[k] * x_list[n - k]

    return int(result)


if __name__ == '__main__':
    x_list = [2, 2]

    while True:
        try:
            n_input = int(input("Количество солдат: ")) - 1

            match n_input:
                case 0 | 1:
                    print(f'Количество неправильных шеренг: 0')
                case _:
                    print(f'Количество неправильных шеренг: {recursion(n_input)}')

        except ValueError:
            print("Вы должны ввести целое число. Пожалуйста попробуйте еще раз.")
            continue
        break
