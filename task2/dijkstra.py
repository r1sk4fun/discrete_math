def dijkstra(graph, start, end):
    """The main function to find the shortest path using Dijkstra's algorithm"""

    # empty dictionary to hold distances
    distances = {}
    # list of vertices in path to current vertex
    predecessors = {}

    # get all the nodes that need to be assessed
    to_assess = graph.keys()

    # set all initial distances to infinity
    #  and no predecessor for any node
    for node in graph:
        distances[node] = float('inf')
        predecessors[node] = None

    # set the initial collection of
    # permanently labelled nodes to be empty
    sp_set = []

    # set the distance from the start node to be 0
    distances[start] = 0

    # as long as there are still nodes to assess:
    while len(sp_set) < len(to_assess):

        # chop out any nodes with a permanent label
        still_in = {node: distances[node] for node in [node for node in to_assess if node not in sp_set]}

        # find the closest node to the current node
        closest = min(still_in, key=distances.get)

        # and add it to the permanently labelled nodes
        sp_set.append(closest)

        # then for all the neighbours of
        # the closest node (that was just added to
        # the permanent set)
        for node in graph[closest]:
            # if a shorter path to that node can be found
            if distances[node] > distances[closest] + \
                    graph[closest][node]:
                # update the distance with
                # that shorter distance
                distances[node] = distances[closest] + \
                                  graph[closest][node]

                # set the predecessor for that node
                predecessors[node] = closest

    # once the loop is complete the final
    # path needs to be calculated - this can
    # be done by backtracking through the predecessors
    path = [end]
    while start not in path:
        path.append(predecessors[path[-1]])

    # return the path in order start -> end, and it's cost
    return path[::-1], distances[end]


def make_graph(filename):
    """A function that create graph from given file"""
    created_graph = {}
    with open(filename) as file:
        for row in file:
            r = row.strip().split('\t')
            label = r.pop(0)
            neighbors = {v: int(length) for v, length in [e.split(',') for e in r]}
            created_graph[label] = neighbors
    return created_graph


if __name__ == '__main__':
    init_graph = make_graph("graph.txt")
    result_path, distance = dijkstra(init_graph, start='1', end='6')
    print(f"Path: {result_path}\nDistance: {distance}")
