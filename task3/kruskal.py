class Graph:
    def __init__(self, filename: str):
        self.filename = filename
        self.graph_dict = {}

    def create_graph(self):
        """A function that create graph from given file"""
        created_graph = {}
        with open(self.filename) as file:
            for row in file:
                r = row.strip().split('\t')
                label = r.pop(0)
                neighbors = {v: int(length) for v, length in [e.split(',') for e in r]}
                created_graph[label] = neighbors
        self.graph_dict.update(created_graph)

    def vertices(self):
        """Gives number of vertices"""
        vertices = []
        for u in self.graph_dict:
            if u not in vertices:
                vertices.append(u)
            for v in self.graph_dict[u]:
                if v not in vertices:
                    vertices.append(v)
        return vertices

    def generate_edges_weight_list(self):
        """A function that rebuild dictionary into list"""
        graph = []
        for u in self.graph_dict:
            for v in self.graph_dict[u]:
                for t in range(self.graph_dict[u][v]):
                    w = self.graph_dict[u][v]
                    edge_weight = [u, v, w]
                    if [u, v, w] not in graph:
                        graph.append(edge_weight)
        return graph

    def find(self, parent, vertex):
        """A utility function to find set of an element i"""
        if parent[vertex] != vertex:
            parent[vertex] = self.find(parent, parent[vertex])
        return parent[vertex]

    def union(self, parent, rank, x, y):
        """A function that does union of two sets of x and y"""
        x_root = self.find(parent, x)
        y_root = self.find(parent, y)

        # Attach smaller rank tree under root of
        # high rank tree (Union by Rank)
        if rank[x_root] < rank[y_root]:
            parent[x_root] = y_root
        elif rank[x_root] > rank[y_root]:
            parent[y_root] = x_root

        # If ranks are same, then make one as root
        # and increment its rank by one
        else:
            parent[y_root] = x_root
            rank[x_root] += 1

    def kruskal_algorithm(self):
        """The main function to construct MST using Kruskal's algorithm"""

        result = []  # This will store the resultant MST

        i = 0  # An index variable, used for sorted edges
        edges = 0  # An index variable, used for result[]

        # Step 1:  Sort all the edges in non-decreasing
        # order of their weight.

        graph_ = self.generate_edges_weight_list()
        graph_ = sorted(graph_, key=lambda item: item[2])

        parent = {}
        rank = {}

        vertices = self.vertices()
        vertices_len = len(vertices)

        # Create vertices_len subsets with single elements
        for vertex in vertices:
            parent[vertex] = vertex
            rank[vertex] = vertex

        # Number of edges to be taken is equal to vertices_len-1
        while edges < vertices_len - 1:

            # Step 2: Pick the smallest edge and increment
            # the index for next iteration
            u, v, w = graph_[i]
            i += 1
            x = self.find(parent, u)
            y = self.find(parent, v)

            # If including this edge doesn't cause cycle,
            # include it in result and increment the index
            # of result for next edge
            if x != y:
                edges += 1
                result.append([u, v, w])
                self.union(parent, rank, x, y)
                # Else discard the edge

        total_weight = 0
        for u, v, weight in result:
            total_weight += weight
            print("%s - %s: %d" % (u, v, weight))
        print(f"Total weight: {total_weight}")


if __name__ == '__main__':
    g = Graph("graph.txt")
    g.create_graph()
    g.kruskal_algorithm()
